// import mongoose from './mongoose
const mongoose = require('mongoose');
// import schema from './schema
const Schema = mongoose.Schema
// khai báo schema
const userSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    name: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: false,
    },
    address: {
        street: {
            type: String,
            required: true,
        },
        suite: {
            type: String,
            required: true,
        },
        city: {
            type: String,
            required: true,
        },
        zipcode: {
            type: String,
            required: true,
        },
        geo: {
            lat: {
                type: Number,
                required: true,
            },
            lng: {
                type: Number,
                required: true,
            },
        }
    },
    phone: {
        type: String,
        required: true,
    },
    website: {
        type: String,
    },
    company: {
        name: {
            type: String,
            required: false,
        },
        catchPhrase: {
            type: String,
            required: true,
        },
        bs: {
            type: String,
            required: true,
        }
    }
})
//export model
module.exports = mongoose.model("user", userSchema);