// import mongoose from './mongoose
const mongoose = require('mongoose');
// import model from './model   
const photoModel = require('../model/photoModel');
// Post create new user
const createPhoto = (request, response) => {
    let body = request.body;
    if (!body.title) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "title is required"
        })
    }
    if (!body.url) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "url is required"
        })
    }
    if (!body.thumbnailUrl) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "thumbnailUrl is required"
        })
    }
    //B3: Sư dụng cơ sở dữ liệu
    let photoCreate = {
        _id: mongoose.Types.ObjectId(),
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl,
    }
    photoModel.create(photoCreate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create photo successfully",
                data: data
            })
        }
    })
}
// get all 
const getAllPhoto = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    photoModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all photo success",
                data: data
            })
        }
    })
}
// put all 
const putPhoto = (request, response) => {
    //B1: thu thập dữ liệu
    let photoId = request.params.photoId;
    let body = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "photo ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updatePhoto = {
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl,
    }
    photoModel.findByIdAndUpdate(photoId, updatePhoto, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update photo success",
                data: data
            })
        }
    })
}
// getUserById all 
const PhotoGetById = (request, response) => {
    //B1: thu thập dữ liệu
    let photoId = request.params.photoId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: " photo ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    photoModel.findById(photoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get photo by id success",
                data: data
            })
        }
    })
}
// deleteUser all 
const deletePhotoById = (request, response) => {
    //B1: thu thập dữ liệu
    let photoId = request.params.photoId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: " photo ID is not valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    photoModel.findOneAndDelete(photoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete photo success"
            })
        }
    })
}
// post of userid
//GET POSTS OF USER
const getPhotoOfUser = (request, response) => {
    //B1: thu thập dữ liệu
    let albumId = request.params.albumId;

    let condition = {};

    if (albumId) {
        condition.albumId = albumId;
    }

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "album ID is not valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    photoModel.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All photos Of albums Id: " + userId,
                data: data
            })
        }
    })
}
//getAllPost
const getAllPhotoQuery = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    let albumId = request.query.albumId

    photoModel.find(albumId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all photos success",
                data: data
            })
        }
    })
}
//export 
module.exports = {
    createPhoto, getAllPhoto, putPhoto, PhotoGetById,
    deletePhotoById, getPhotoOfUser, getAllPhotoQuery
}